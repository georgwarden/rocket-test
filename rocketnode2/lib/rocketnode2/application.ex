defmodule Rocketnode2.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      Rocketnode2Web.Endpoint,
      Rocketnode2.Persistence,
      Rocketnode2.Rabbit
    ]

    opts = [strategy: :one_for_one, name: Rocketnode2.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    Rocketnode2Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
