defmodule Rocketnode2.Rabbit do
    use GenServer
    require Logger

    @output "telegram_out"
    @inpoot "web_out"
    @rabbit_host "amqp://rocket:rocket123@rocket-test_rabbit_1"

    def start_link(_) do
        GenServer.start_link(__MODULE__, :empty, name: __MODULE__)
    end

    def init(_) do
        with {:ok, connection} <- AMQP.Connection.open(@rabbit_host),
             {:ok, channel} <- AMQP.Channel.open(connection) do
                 AMQP.Queue.declare(channel, @output)
                 AMQP.Queue.declare(channel, @inpoot)
                 receiver_process = Kernel.spawn(&Rocketnode2.Rabbit.Receiver.consume_next/0)
                 AMQP.Basic.consume(channel, @inpoot, receiver_process, no_ack: true)
                 {:ok, channel}
            else
                error -> {:stop, "Couldn't connect to RabbitMQ: #{inspect(error)}"}
            end
    end

    def handle_cast({:message, message}, channel) do
	    Logger.debug("sending to rabbit: #{inspect message}")
        AMQP.Basic.publish channel, "", @output, message
        {:noreply, channel}
    end

    def send(message) do
        GenServer.cast(__MODULE__, {:message, message})
    end

end

defmodule Rocketnode2.Rabbit.Receiver do
    
    def consume_next() do
    receive do
        {:basic_deliver, message, _meta} ->
            Rocketnode2.Persistence.write_message(message)
            Rocketnode2Web.MessagesChannel.broadcast(message)
            consume_next()
        end
    end

end
