defmodule Rocketnode2.Persistence do
    use GenServer
    require Logger

    def start_link(_) do
        GenServer.start_link(__MODULE__, :nothing, name: __MODULE__)
    end

    def init(_) do
        {:ok, file} = File.open("/app/stored.txt", [:read, :append, :utf8])
	    Logger.debug("opened AO-log: #{inspect file}")
        {:ok, file} # clarity over non-clarity
    end

    def handle_cast({:write, message}, file) do
	Logger.debug("writing #{message} to log")
        IO.write(file, message <> "\n")
	    File.close(file)
	    {:ok, reopened} = File.open("/app/stored.txt", [:read, :append, :utf8])
	    Logger.debug("written #{message} to log")
        {:noreply, reopened}
    end

    # this one operation is actually blocking like hell, fix later
    def handle_call({:read_all}, _from, file) do
	    Logger.debug("reading from #{inspect file}")
        history = IO.stream(file, :line)
	    |> Enum.map(&String.trim/1)
        |> Enum.reduce([], fn line, history -> history ++ [line] end)
	    Logger.debug("returning #{inspect history}")
        {:reply, history, file}
    end
    
    def write_message(message) do
        GenServer.cast(__MODULE__, {:write, message})
    end

    def read_all() do
        GenServer.call(__MODULE__, {:read_all})
    end

end
