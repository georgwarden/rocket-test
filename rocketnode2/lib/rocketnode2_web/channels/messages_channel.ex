defmodule Rocketnode2Web.MessagesChannel do
    use Phoenix.Channel
    require Logger

    @topic_name "messages:public"
    @message_event "new_message"

    def join(@topic_name, _, socket) do
        history = Rocketnode2.Persistence.read_all
        {:ok, %{history: history}, socket}
    end

    def handle_in(@message_event, %{"body" => content}, socket) do
	Rocketnode2.Persistence.write_message(content)
        broadcast(socket, @message_event, %{body: content})
        Rocketnode2.Rabbit.send content
        {:noreply, socket}
    end

    def broadcast(message) do
        Rocketnode2Web.Endpoint.broadcast @topic_name, @message_event, %{body: message}
    end

    def topic_name(), do: @topic_name
    def message_event(), do: @message_event

end
