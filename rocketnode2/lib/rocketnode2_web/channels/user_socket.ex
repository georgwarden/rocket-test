defmodule Rocketnode2Web.UserSocket do
  use Phoenix.Socket

  channel "messages:*", Rocketnode2Web.MessagesChannel

  def connect(_params, socket, _connect_info) do
    {:ok, socket}
  end

  def id(_socket), do: nil

end
