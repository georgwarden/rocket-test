defmodule Rocketnode2Web.PageController do
  use Rocketnode2Web, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
