defmodule Telegram.Bot do
    use GenServer
    require Logger
    alias Telegram.Data

    def start_link(_) do
        GenServer.start_link(__MODULE__, -314820972, name: __MODULE__)
    end

    @impl true
    def init(chat_id) do
        Kernel.spawn(&poll/0)
        {:ok, chat_id}
    end

    @impl true
    def handle_cast({:send, message}, chat_id) do
        Logger.debug "sending #{message}"
        Nadia.send_message chat_id, message
        {:noreply, chat_id}
    end

    @impl true
    def handle_cast(rest, state) do
        Logger.debug "unhandled cast of #{rest}, state intact"
        {:noreply, state}
    end

    ### api
    def send_message(message) do
        GenServer.cast __MODULE__, {:send, message}
    end

    ### receiveing
    defp poll do
        poll(0)
    end

    defp poll(offset) do
        case Nadia.get_updates(offset: offset, timeout: 86_400_000) do
            {:ok, []} ->
                poll(offset)
            {:ok, updates} ->
                Logger.debug("received updates: #{inspect updates}")
                %{update_id: update_id} = List.last(updates)
                Data.extract_message_list(updates)
                |> Enum.each(&Coordinator.message_from_telegram/1)
                poll(update_id + 1)
            {:error, error} ->
                Logger.error "Nadia wasn't able to retrieve updates: #{inspect error}"
                poll(offset)
        end
    end

end
