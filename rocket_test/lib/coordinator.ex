defmodule Coordinator do
    alias Telegram.Bot
    alias Rabbit.Producer

    def message_from_telegram(message) do
        Producer.send message
    end

    def message_from_user(message) do
        Bot.send_message message
    end

end