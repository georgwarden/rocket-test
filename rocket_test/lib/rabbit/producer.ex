defmodule Rabbit.Producer do
    use GenServer
    require Logger
    
    @inpoot "telegram_out"
    @output "web_out"
    @rabbit_host "amqp://rocket:rocket123@rocket-test_rabbit_1"

    def start_link(_) do
        GenServer.start_link(__MODULE__, [], name: __MODULE__)
    end

    @impl true
    def init(_) do
        with {:ok, connection} <- AMQP.Connection.open(@rabbit_host),
             {:ok, channel} <- AMQP.Channel.open connection do
                AMQP.Queue.declare(channel, @output)
                AMQP.Queue.declare(channel, @inpoot)
                receiver_process = Kernel.spawn(&Rabbit.Receiver.consume_next/0)
                AMQP.Basic.consume(channel, @inpoot, receiver_process, no_ack: true)
                {:ok, channel}
            else
                error -> {:error, "Can't connect to RabbitMQ: #{inspect error}"}
            end
    end

    @impl true
    def handle_cast({:send, message}, channel) do
	Logger.debug("sending #{inspect message} to rabbit")
        AMQP.Basic.publish channel, "", @output, message
        {:noreply, channel}
    end

    @impl true
    def handle_cast(rest, state) do
        Logger.debug "unhandled cast of #{rest}, state intact"
        {:noreply, state}
    end

    ### api
    def send(message), do: GenServer.cast __MODULE__, {:send, message}

end

defmodule Rabbit.Receiver do
    require Logger

    def consume_next() do
    receive do
        {:basic_deliver, message, _meta} ->
            Logger.debug("message from rabbit: #{message}")
            Coordinator.message_from_user(message)
            consume_next()
        end
    end

end
